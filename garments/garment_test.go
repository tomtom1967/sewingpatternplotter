package garments

import (
	"math"
	"os"
	"path/filepath"
	"reflect"
	"testing"

	svg "github.com/ajstarks/svgo"
	"github.com/google/go-cmp/cmp"
)

// Dave Cheny on table driven tests
//https://dave.cheney.net/2019/05/07/prefer-table-driven-tests
// I have not implemented all his tips. But will try this making more tests

func TestPointDistance(t *testing.T) {
	type test struct {
		p1, p2 Point
		want   float64
	}

	tests := []test{
		{p1: Point{1, 1, "p1"}, p2: Point{1, 2, "p1"}, want: 1},
		{p1: Point{1, 1, "p1"}, p2: Point{1, 3, "p1"}, want: 2},
		{p1: Point{1, 1, "p1"}, p2: Point{3, 3, "p1"}, want: 2.8284271247461903},
		{p1: Point{1, 1, "p1"}, p2: Point{11, 22, "p1"}, want: 23.259406699226016},
		{p1: Point{4, 5, "p1"}, p2: Point{2, 7, "p1"}, want: 2.8284271247461903},
	}

	for i, test := range tests {
		want := test.want
		got := test.p1.distance(test.p2)
		if !reflect.DeepEqual(want, got) {
			t.Errorf("test %d: expected: %v, got: %v", i+1, want, got)
		}
	}
}

func TestPointDirection(t *testing.T) {
	type test struct {
		p1, p2 Point
		want   string
	}

	tests := []test{
		{p1: Point{1, 1, "p1"}, p2: Point{1, 2, "p1"}, want: "d"},
		{p1: Point{1, 1, "p1"}, p2: Point{2, 1, "p1"}, want: "r"},
		{p1: Point{1, 1, "p1"}, p2: Point{3, 3, "p1"}, want: "rd"},
		{p1: Point{4, 4, "p1"}, p2: Point{1, 4, "p1"}, want: "l"},
		{p1: Point{4, 5, "p1"}, p2: Point{2, 4, "p1"}, want: "lu"},
		{p1: Point{4, 5, "p1"}, p2: Point{4, 4, "p1"}, want: "u"},
		{p1: Point{4, 5, "p1"}, p2: Point{5, 4, "p1"}, want: "ru"},
	}

	for i, test := range tests {
		want := test.want
		got := test.p1.direction(test.p2)
		if !reflect.DeepEqual(want, got) {
			t.Errorf("test %d: expected: %#v, got: %#v", i+1, want, got)
		}
	}
}

func TestPointDiagonal(t *testing.T) {
	type test struct {
		p1             Point
		radius, degree float64
		want           Point
	}

	tests := []test{
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 0, want: Point{11, 1, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 10, want: Point{11, 3, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 20, want: Point{10, 4, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 30, want: Point{10, 6, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 40, want: Point{9, 7, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 50, want: Point{7, 9, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 60, want: Point{6, 10, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 70, want: Point{4, 10, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 80, want: Point{3, 11, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 90, want: Point{1, 11, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 100, want: Point{-1, 11, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 110, want: Point{-2, 10, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 120, want: Point{-4, 10, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 130, want: Point{-5, 9, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 140, want: Point{-7, 7, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 150, want: Point{-8, 6, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 160, want: Point{-8, 4, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 170, want: Point{-9, 3, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 180, want: Point{-9, 1, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 190, want: Point{-9, -1, "p1d"}},

		{p1: Point{1, 1, "p1"}, radius: 10, degree: 405, want: Point{8, 8, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 20, degree: 45, want: Point{15, 15, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 20, want: Point{10, 4, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 20, degree: 20, want: Point{20, 8, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: 405, want: Point{8, 8, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: 10, degree: -405, want: Point{8, -6, "p1d"}},
		{p1: Point{1, 1, "p1"}, radius: -20, degree: -45, want: Point{-13, 15, "p1d"}},
		{p1: Point{20, 1, "p1"}, radius: -20, degree: -45, want: Point{6, 15, "p1d"}},
	}

	for i, test := range tests {
		want := test.want
		got := test.p1.diagonal(test.radius, test.degree)
		if !reflect.DeepEqual(want, got) {
			t.Errorf("test %d: expected: %#v, got: %#v", i+1, want, got)
		}
	}
}

func TestPointCalculateAngelOfTwoLines(t *testing.T) {
	tests := map[string]struct {
		p1, p2, p3 Point
		want       float64
	}{
		"nulgraden":  {p1: Point{10, 10, "p1"}, p2: Point{10, 20, "p2"}, p3: Point{10, 10, "p3"}, want: 0},
		"diagonaal":  {p1: Point{10, 10, "p1"}, p2: Point{10, 20, "p2"}, p3: Point{20, 10, "p3"}, want: 45},
		"haaks":      {p1: Point{10, 10, "p1"}, p2: Point{10, 20, "p2"}, p3: Point{20, 20, "p3"}, want: 90},
		"haaks2":     {p1: Point{10, 10, "p1"}, p2: Point{10, 20, "p2"}, p3: Point{0, 20, "p3"}, want: 90},
		"rechteLijn": {p1: Point{10, 10, "p1"}, p2: Point{20, 10, "p2"}, p3: Point{30, 10, "p3"}, want: 180},
		"wijdehoek":  {p1: Point{10, 20, "p1"}, p2: Point{20, 20, "p2"}, p3: Point{30, 10, "p3"}, want: 135},
		"wijdehoek2": {p1: Point{10, 20, "p1"}, p2: Point{20, 20, "p2"}, p3: Point{30, 19, "p3"}, want: 174},
		"wijdehoek3": {p1: Point{10, 20, "p1"}, p2: Point{20, 20, "p2"}, p3: Point{30, 21, "p3"}, want: 174},
		"wijdehoek4": {p1: Point{10, 20, "p1"}, p2: Point{20, 20, "p2"}, p3: Point{100, 21, "p3"}, want: 179},
	}
	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			// rads to degree conversion: rad×180°/π
			got := math.Round(tc.p2.angel(tc.p1, tc.p3))
			diff := cmp.Diff(tc.want, got)
			if diff != "" {
				t.Fatalf(diff)
			}
		})
	}
}

func TestLineDegree(t *testing.T) {
	test := []struct {
		p1, p2 Point
		want   float64
	}{
		{p1: Point{0, 0, "p1"}, p2: Point{30, 0, "p2"}, want: float64(0)},
		{p1: Point{0, 0, "p1"}, p2: Point{30, 30, "p2"}, want: float64(45)},
		{p1: Point{0, 0, "p1"}, p2: Point{0, 30, "p2"}, want: float64(90)},
		{p1: Point{0, 0, "p1"}, p2: Point{-30, 30, "p2"}, want: float64(135)},
		{p1: Point{0, 0, "p1"}, p2: Point{-30, 0, "p2"}, want: float64(180)},
		{p1: Point{0, 0, "p1"}, p2: Point{-30, -30, "p2"}, want: float64(225)},
		{p1: Point{0, 0, "p1"}, p2: Point{0, -30, "p2"}, want: float64(270)},
		{p1: Point{0, 0, "p1"}, p2: Point{30, -30, "p2"}, want: float64(315)},

		{p1: Point{0, 0, "p1"}, p2: Point{5, 9, "p2"}, want: float64(60.94539590092286)},
	}
	for i, tc := range test {
		got := tc.p1.lineDegree(tc.p2)
		if got != tc.want {
			t.Fatalf("test %d: expected: %v, got: %v", i+1, tc.want, got)
		}
	}
}

func TestCalculateHemPoint(t *testing.T) {
	tests := []struct {
		p1, p2, p3 Point
		gravity    Point
		want       Point
	}{
		//90 degreen angels
		{p1: Point{10, 0, "p1"}, p2: Point{0, 0, "p2"}, p3: Point{0, 10, "p3"}, gravity: Point{15, 15, "gp1"}, want: Point{7, 7, "p2d"}},   // 1
		{p1: Point{0, 10, "p1"}, p2: Point{0, 0, "p2"}, p3: Point{10, 0, "p3"}, gravity: Point{15, 15, "gp1"}, want: Point{7, 77, "p2d"}},  // 2
		{p1: Point{0, 0, "p1"}, p2: Point{10, 0, "p2"}, p3: Point{10, 10, "p3"}, gravity: Point{5, 5, "gp1"}, want: Point{0, 10, "p2d"}},   // 3
		{p1: Point{10, 10, "p1"}, p2: Point{10, 0, "p2"}, p3: Point{0, 0, "p3"}, gravity: Point{5, 5, "gp1"}, want: Point{0, 10, "p2d"}},   // 4
		{p1: Point{0, 0, "p1"}, p2: Point{10, 0, "p2"}, p3: Point{10, 10, "p3"}, gravity: Point{15, 5, "gp1"}, want: Point{20, 10, "p2d"}}, // 5
		{p1: Point{10, 10, "p1"}, p2: Point{10, 0, "p2"}, p3: Point{0, 0, "p3"}, gravity: Point{15, 5, "gp1"}, want: Point{20, 10, "p2d"}}, // 6
		// Wide angel
		{p1: Point{10, 0, "p1"}, p2: Point{0, 0, "p2"}, p3: Point{-10, 10, "p3"}, gravity: Point{15, 15, "gp1"}, want: Point{4, 9, "p2d"}}, // 7
		{p1: Point{0, 10, "p1"}, p2: Point{0, 0, "p2"}, p3: Point{10, -10, "p3"}, gravity: Point{15, 15, "gp1"}, want: Point{4, 9, "p2d"}}, // 8
		// straight line
		{p1: Point{0, 0, "p1"}, p2: Point{10, 0, "p2"}, p3: Point{20, 0, "p3"}, gravity: Point{15, 15, "gp1"}, want: Point{10, 10, "p2d"}},     // 9
		{p1: Point{0, 10, "p1"}, p2: Point{0, 20, "p2"}, p3: Point{0, 30, "p3"}, gravity: Point{15, 15, "gp1"}, want: Point{10, 20, "p2d"}},    // 10
		{p1: Point{0, 0, "p1"}, p2: Point{20, 20, "p2"}, p3: Point{30, 30, "p3"}, gravity: Point{15, 15, "gp1"}, want: Point{20, 10, "p2d"}},   // 11
		{p1: Point{0, 0, "p1"}, p2: Point{20, 20, "p2"}, p3: Point{30, 30, "p3"}, gravity: Point{15, 15, "gp1"}, want: Point{20, 10, "p2d"}},   // 12
		{p1: Point{0, -5, "p1"}, p2: Point{10, -5, "p2"}, p3: Point{84, 0, "p3"}, gravity: Point{40, -10, "gp1"}, want: Point{10, -15, "p2d"}}, // 13
		{p1: Point{100, 10, "p6"}, p2: Point{50, 10, "p7"}, p3: Point{10, 10, "p8"}, gravity: Point{50, 0, "gp1"}, want: Point{50, 20, "p7d"}}, // 14
	}

	for i, tc := range tests {
		got := tc.p2.hemPoint(tc.gravity, tc.p1, tc.p3)
		if got != tc.want {
			t.Fatalf("test %d: expected: %v, got: %v", i+1, tc.want, got)
		}
	}
}

func TestHemline(t *testing.T) {
	//g := Garment{}
	p := newPart("square")
	p.add(0, 20, "p0")

	p.add(0, 40, "p1")
	p.add(-15, 60, "p2")
	p.add(0, 80, "p3")

	p.add(0, 120, "p4")
	p.add(40, 120, "p5")
	p.add(40, 60, "p6")
	p.add(60, 40, "p7")
	p.add(80, 40, "p8")
	p.add(100, 60, "p9")
	p.add(100, 120, "p10")
	p.add(150, 120, "p11")

	p.add(150, 80, "p12")
	p.add(165, 60, "p13")
	p.add(150, 40, "p14")

	p.add(150, 20, "p15")
	p.add(130, 0, "p16")
	p.add(110, 0, "p17")
	p.add(110, -30, "p18")
	p.add(40, -30, "p19")
	p.add(40, 0, "p20")
	p.add(20, 0, "p21")

	sq := newOutline("square", styleCuttingEdge)
	sq.add(p.pm["p0"])
	sq.add(p.pm["p1"])
	sq.add(p.pm["p2"])
	sq.add(p.pm["p3"])
	sq.add(p.pm["p4"])
	sq.add(p.pm["p5"])
	sq.add(p.pm["p6"])
	sq.add(p.pm["p7"])
	sq.add(p.pm["p8"])
	sq.add(p.pm["p9"])
	sq.add(p.pm["p10"])
	sq.add(p.pm["p11"])

	sq.add(p.pm["p12"])
	sq.add(p.pm["p13"])
	sq.add(p.pm["p14"])
	sq.add(p.pm["p15"])
	sq.add(p.pm["p16"])
	sq.add(p.pm["p17"])
	sq.add(p.pm["p18"])
	sq.add(p.pm["p19"])
	sq.add(p.pm["p20"])
	sq.add(p.pm["p21"])
	sq.add(p.pm["p0"])
	sq.addGravity(29, 30)
	sq.addGravity(105, 30)
	sq.addGravity(70, 1)

	p.addOutline(sq)
	p.addOutline(sq.calculateHemPoints())
	dir, _ := filepath.Abs("./")
	f, err := os.Create(dir + "/test.svg")
	if err != nil {
		t.Errorf("Unable to open file: %s/test.svg", dir)
	}
	defer f.Sync()
	defer f.Close()
	canvas := svg.New(f)
	canvas.StartviewUnit(300, 300, "mm", 0, 0, 300, 300)
	canvas.Rect(0, 0, 300, 300, `fill="white"`)
	canvas.Grid(0, 0, 300, 300, 100, "stroke:black;opacity:0.1")
	p.Draw(canvas, 50, 100)
	canvas.End()
}
