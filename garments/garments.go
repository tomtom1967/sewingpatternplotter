/*
Garment will enables you to create and print sewing patterns for made to messure garments.

A garment is saved in a real size svg file, that con be converted to A4 printeble multie part sheets. Enabeling you to print the sewing pattern at
home using your regular printer.

A sewing pattern exists of one ore more named parts. These parts can have one ore more outlines. The parts are a collection of points and outlines.
Points are two demensional (x,y). These point's make up a part and are used fore refrence to and from eachother, and for creating the outlines.
The outlines wil use certain points from the part to draw certain shapes that are saved in a svg file. The parts in the outline should be connected
counterclockwise. The outlines support "automatical hemming" You can draw your pattern with (on the cutting edge) or without hem, and then automatically include
the hem or the cutting edge.

Example of using this package:
  cussion := NewGarment("cussion")
  cussion.pm.add(0,0,"p0")
  cussion.pm.add(0,100,"p1")
  cussion.pm.add(100,100,"p2")
  cussion.pm.add(100,0,"p3")

enz enz...
*/

package garments

import (
	"fmt"
	"log"
	"math"
	"strings"

	svg "github.com/ajstarks/svgo"
)

// This patter uses a 10 mm hem
const hem = 10

// MesurmentType makes ints that have meaning
type MesurmentType int

// Constants used for identifying different messurement types.
// When updating these const you should run the command below on the command line:
//   stringer -type=MesurmentType
// See: https://godoc.org/golang.org/x/tools/cmd/stringer
const (
	Name MesurmentType = iota
	Date
	NeckSize
	Chest
	SyncDepth
	BackNeckToWaist
	HalfBack
	ShirtLength
	SleeveLength
	CuffSize
	CuffDepth
	CollarDepth
)

// SVG styles used to differentiate between types of lines
var styleCuttingEdge = []string{`fill="none"`, `stroke="black"`, `stroke-width="1"`}
var styleDart = []string{`fill="none"`, `stroke="green"`, `stroke-dasharray="5,5,1,5"`, `stroke-width="1"`}
var styleFoldline = []string{`fill="none"`, `stroke="blue"`, `stroke-dasharray="1,5"`, `stroke-width="1"`}
var styleCenterLine = []string{`fill="none"`, `stroke="blue"`, `stroke-dasharray="5,1"`, `stroke-width="1"`}
var styleHem = []string{`fill="none"`, `stroke="gray"`, `stroke-dasharray="5,5"`, `stroke-width="1"`}

// Garment is een struct met messurments
// En met map[string]Part
// De Parts bestaan partMap en een of meer Outlines, die de aparte stukken van de PointMap bevatten
// De partMap is een PointList
// De Outline heeft een Naam, Style en PointList en een gravity point
type Garment struct {
	name        string
	description string
	mesurements Mesurments
	parts       map[string]Part
}

// new garment
func new(name string) Garment {
	return Garment{name: name}
}

// addPart adds a part to the garment
func (g *Garment) addPart(p Part) {
	g.parts[p.name] = p
}

// part gets a part by name from the garment
func (g *Garment) part(name string) Part {
	return g.parts[name]
}

// Draw puts all the parts on the canvas
func (g *Garment) Draw(c *svg.SVG, xOffset, yOffset int) {
	for _, p := range g.parts {
		p.Draw(c, xOffset, yOffset)
	}
	c.End()
}

// Garment is the interfase that every new garment should implement
// type garment interface {
// 	// Draw puts the garment on the cancas
// 	Draw(c *svg.SVG)

// 	AddPart(s string)
// 	// A0 841 x 1189 mm
// 	// 2A0 1189 x 1682 mm
// 	CanvasWidth() int
// 	CanvasHeight() int
// }

// Mesurments contain the user input
type Mesurments map[MesurmentType]int

// Info contains the user input
type Info map[MesurmentType]string

// Line is a first attemt to translete the pattern book in to code
// They work as sentences that can be printed
// type Line struct {
// 	from Point
// 	to   Point
// }

// Lines is the slice where I collect the lines in.
// Having the lines in a slice I can print them on screen one by one
//type Lines []Line

// Part holds the points that make a pattern part
type Part struct {
	name    string
	pm      PointMap
	outline []Outline
}

func newPart(name string) Part {
	return Part{name: name, pm: PointMap{}, outline: []Outline{}}
}

func (p *Part) addPoint(pt Point) {
	p.pm.addPoint(pt)
}

func (p *Part) add(x, y int, name string) {
	p.pm.add(x, y, name)
}

func (p *Part) addOutline(ol Outline) {
	p.outline = append(p.outline, ol)
}

// PointMap holds points and there names
type PointMap map[string]Point

func (pm PointMap) addPoint(p Point) {
	pm[p.name] = p
}
func (pm PointMap) add(x, y int, name string) {
	pm[name] = Point{x, y, name}
}

// Draw draws the part on the canvas
func (p *Part) Draw(c *svg.SVG, xOffset, yOffset int) {
	for _, ol := range p.outline {
		pointsToPolyline(ol.withOffset(xOffset, yOffset), c, ol.style...)
		addDotsToCanvas(ol.withOffset(xOffset, yOffset), c, `stroke-width="2"`, `fill="red"`)
		pointNrToCanvas(ol.withOffset(xOffset, yOffset), c)
		// Add gravity point in the drawing
		if len(ol.gravity) > 0 {
			for _, g := range ol.gravity {
				c.Circle(g.x+xOffset, g.y+yOffset, 2, `stroke-width="2"`, `fill="blue"`)
			}
		}
	}
}

// Point contains the x and y position in a asumed matrix
// connecting these Points in a certain way the pattern can be drawn.
// Points are usefull because they also alow you to calculate distance between Points.
type Point struct {
	x    int
	y    int
	name string
}

// distance calculates the distance of the Point to the given Point.
func (p Point) distance(pp Point) float64 {
	if len(p.name) == 0 {
		log.Fatalf("Empty Point in distance p, x:%d, y:%d\n", p.x, p.y)
	}
	if len(pp.name) == 0 {
		log.Fatalf("Empty Point in distance pp, x:%d, y:%d\n", p.x, p.y)
	}

	return math.Sqrt(math.Pow(float64(p.x)-float64(pp.x), 2) + math.Pow(float64(p.y)-float64(pp.y), 2))
}

// distanceInt gives the distance in integers
func (p Point) distanceInt(pp Point) int {
	return int(math.Round(p.distance(pp)))
}

// direction discribes the direction the given point is to the Point
func (p Point) direction(pp Point) string {
	if len(p.name) == 0 {
		log.Fatalf("Empty Point in direction point, x:%d, y:%d\n", p.x, p.y)
	}
	if len(pp.name) == 0 {
		log.Fatalf("Empty Point in direction variable, x:%d, y:%d\n", pp.x, pp.y)
	}

	d := ""
	if p.x < pp.x {
		d += "r"
	}
	if p.x > pp.x {
		d += "l"
	}
	if p.y < pp.y {
		d += "d"
	}
	if p.y > pp.y {
		d += "u"
	}
	return d
}

// diagonal calculates the distance of the x and y to get to a certain diagonal distance
// In pattern making this can be used to smouth out the corners in a drawing.
// Creating a Point in a certain diagonal distance fills in the gap's of a straight line.
// r is the radius aka distance, d is the degree
// The degree starts on the x-as as zerro. That is why 45 is pointing right down

// The direction of this function is different than the direction of the lineDegree function.
// This function moves clockwise. The lineDegree function moves counter clockwise
func (p Point) diagonal(radius float64, angle float64) Point {
	if len(p.name) == 0 {
		log.Fatalf("Empty Point in diagonal, x:%d, y:%d\n", p.x, p.y)
	}
	x := int(math.Round(radius*math.Cos(angle*(math.Pi/180)) + float64(p.x)))
	y := int(math.Round(radius*math.Sin(angle*(math.Pi/180)) + float64(p.y)))
	return Point{x, y, p.name + "d"}
}

// diagonalRightUp calculates the Point's diagonal Point to the right(+) and above(-) the Point
func (p Point) diagonalRightDown(d float64) Point {
	return p.diagonal(d, 45)
}

// diagonalRightDown calculates the Point's diagonal Point to the right(+) and below(+) the Point
func (p Point) diagonalLeftDown(d float64) Point {
	return p.diagonal(d, 135)
}

// diagonalLeftDown calculates the Point's diagonal Point to the left(-) and below(+) the Point
func (p Point) diagonalLeftUp(d float64) Point {
	return p.diagonal(d, 225)
}

// diagonalLeftUp calculates the Point's diagonal Point to the left(-) and above(-) the Point
func (p Point) diagonalRightUp(d float64) Point {
	return p.diagonal(d, 315)
}

func (p *Point) angel(before, after Point) float64 {
	a := p.distance(before)
	c := p.distance(after)
	b := before.distance(after)

	//fmt.Printf("A:%f, B:%f, C:%f Hoek:%f\n", a, b, c, (math.Acos((math.Pow(a, 2)+math.Pow(c, 2)-math.Pow(b, 2))/(2*a*c))*180)/math.Pi)

	return (math.Acos((math.Pow(a, 2)+math.Pow(c, 2)-math.Pow(b, 2))/(2*a*c)) * 180) / math.Pi
}

func (p *Point) lineDegree(pp Point) float64 {

	Ax := float64(pp.x - p.x)
	Ay := float64(pp.y - p.y)
	slope := Ay / Ax

	if math.IsInf(slope, 1) {
		return 90
	}

	degree := math.Atan(slope) * (180 / math.Pi)
	//fmt.Printf("D:%s, Degree:%f, xy:(%d,%d), xy:(%d,%d)\n", p.direction(pp), degree, p.x, p.y, pp.x, pp.y)
	return degree + 180
}

func (p *Point) hemPoint(gravity, before, after Point) Point {
	if len(gravity.name) == 0 {
		log.Fatalf("Empty Point in diagonal, x:%d, y:%d\n", gravity.x, gravity.y)
	}
	if len(before.name) == 0 {
		log.Fatalf("Empty Point in diagonal, x:%d, y:%d\n", before.x, before.y)
	}
	if len(after.name) == 0 {
		log.Fatalf("Empty Point in diagonal, x:%d, y:%d\n", after.x, after.y)
	}

	angle := p.angel(before, after)
	directionBefore := p.direction(before)
	directionAfter := p.direction(after)
	directionGrafity := p.direction(gravity)

	fmt.Printf("Name: %s, Degree: %f, Angel: %f, DB: %s, DG: %s\n", p.name, degree, angle, directionBefore, directionGrafity)

	return p.diagonal(10, angle/2)

	//return Point{p.x, p.y, "TEST"}

	fmt.Printf("p: %s, Hk %f, DG:%s ", p.name, angle, directionGrafity)
	//ss := strings.Split(directionGrafity, "")
	ms := map[string]string{}
	for _, sba := range directionBefore + directionAfter {
		ms[string(sba)] = string(sba)
	}

	// Wegstrepen l
	for k := range ms {
		if k == "l" {
			if _, ok := ms["r"]; ok {
				delete(ms, "r")
				delete(ms, "l")
			}
		}
		if k == "r" {
			if _, ok := ms["l"]; ok {
				delete(ms, "r")
				delete(ms, "l")
			}
		}
		if k == "u" {
			if _, ok := ms["d"]; ok {
				delete(ms, "u")
				delete(ms, "d")
			}
		}
		if k == "d" {
			if _, ok := ms["u"]; ok {
				delete(ms, "u")
				delete(ms, "d")
			}
		}
	}

	dgSlice := strings.Split(directionGrafity, "")
	_, ok1 := ms[dgSlice[0]]
	_, ok2 := ms[dgSlice[1]]

	if !ok1 && !ok2 {
		angle = 360 - angle
		p.name += "*"

	}

	if directionBefore == "u" {
		return p.diagonal(10, -angle/2)
	}
	if directionBefore == "ru" {
		return p.diagonal(10, p.lineDegree(before)-angle)
	}

	// Take the have of the angel, and add it to the before line.
	return p.diagonal(10, p.lineDegree(before)+angle/2)

	//fmt.Printf("N:%s, A:%f, GD:%s, BD:%s, AD:%s\n", p.name, angle, directionGrafity, directionBefore, directionAfter)

	//return *p
}

// Outline contains the points that draw the pattern part
type Outline struct {
	name    string
	style   []string
	list    []Point
	gravity []Point
}

// newOutline continas the points
func newOutline(name string, style []string) Outline {
	return Outline{
		name:  name,
		style: style,
	}
}

// Add one ore more Point's
func (ol *Outline) add(p ...Point) {
	ol.list = append(ol.list, p...)
}

// addGravity adds a gravity point in an outline.
// Gravity points are needed to determine the direction when point's diagonals need to point to the "middel"
// This is used when generating the hem line
func (ol *Outline) addGravity(x, y int) {
	ol.gravity = append(ol.gravity, Point{x, y, fmt.Sprintf("pg%d", len(ol.gravity)+1)})
}

// getNearestGravity Point will return the gravity point that is near the given point
func (ol *Outline) getNearestGravity(p Point) Point {
	if len(ol.gravity) == 1 {
		return ol.gravity[0]
	}
	var np = ol.gravity[0]
	for i := 1; i < len(ol.gravity); i++ {
		if p.distance(ol.gravity[i]) < p.distance(np) {
			np = ol.gravity[i]
		}
	}
	return np
}

// calculateHemPoints calculates the hempoints of an outline with the use of a gravity point
func (ol *Outline) calculateHemPoints() Outline {
	hemOl := newOutline("hem", styleHem)
	fmt.Printf("Outline: %s \n", ol.name)

	// We can't calculate hem points without gravity points,
	// and with less the three outline points
	if len(ol.gravity) > 0 && len(ol.list) >= 3 {
		for i := 0; i < len(ol.list); i++ {

			if i <= len(ol.list)-3 {
				// All three points need to be different
				if ol.list[0+i] == ol.list[1+i] || ol.list[0+i] == ol.list[2+i] {
					continue
				}
				gp := ol.getNearestGravity(ol.list[1+i])
				hemOl.add(ol.list[1+i].hemPoint(gp, ol.list[0+i], ol.list[2+i])) //   calculateHemPoint(gp, , ol.list[1+i], ))
				continue
			}
			if i <= len(ol.list)-2 {
				// All three points need to be different
				if ol.list[i] == ol.list[1+i] || ol.list[i] == ol.list[1] {
					continue
				}
				gp := ol.getNearestGravity(ol.list[i+1])
				hemOl.add((ol.list[i+1].hemPoint(gp, ol.list[i], ol.list[1])))
				continue
			}
			if i <= len(ol.list)-1 {
				// All three points need to be different
				if ol.list[0] == ol.list[1] || ol.list[0] == ol.list[2] {
					continue
				}
				gp := ol.getNearestGravity(ol.list[1])
				hemOl.add(ol.list[1].hemPoint(gp, ol.list[0], ol.list[2]))
				continue
			}
		}
	}
	return hemOl
}

// withOffset gets the points of the outline withe a given offset.
// This enables positioning of the outline
func (ol *Outline) withOffset(xOffset int, yOffset int) Outline {
	withOffset := Outline{
		style: ol.style,
		list:  []Point{},
		name:  ol.name,
	}
	for _, p := range ol.list {
		withOffset.add(Point{p.x + xOffset, p.y + yOffset, p.name})
	}
	return withOffset
}

// calculateBfromCandA does b = √(c²-a²)
// Having the the hypotenuse length and one straight length it calculates the oposite straight side.
// This is usefull because sometimes we have the hypotenuse length and need to translate that to x,y coordinates.
func calculateBfromCandA(c, a int) int {
	return int(math.Round(math.Sqrt(math.Pow(float64(c), 2) - math.Pow(float64(a), 2))))
}
