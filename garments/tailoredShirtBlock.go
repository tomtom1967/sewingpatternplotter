package garments

import (
	"math"

	svg "github.com/ajstarks/svgo"
)

// TailoredShirtBlock is a container that holds the parts's to create the pices that maken a tailord shirt
type TailoredShirtBlock struct {
	m                                                                  Mesurments
	shirt, sleeve, collar, collarStand, placked, testSquare, testRound Part
}

// NewTailoredShirtBlock creates a new tailored shirt block
func NewTailoredShirtBlock(m Mesurments) TailoredShirtBlock {
	ts := TailoredShirtBlock{}
	ts.m = m
	// ts.setPartShirt(m)
	// ts.setPointsSleeve(m)
	// ts.setPointsCollar(m)
	// ts.setPlacked(m)

	ts.setTestSquare()

	return ts
}

// CanvasWidth returns the canvas width needed for this garment
func (g TailoredShirtBlock) CanvasWidth() int {
	//return g.m[Chest] + 200
	return 300
}

// CanvasHeight returns the canvas width needed for this garment
func (g TailoredShirtBlock) CanvasHeight() int {
	//return g.m[ShirtLength] + 150
	return 300
}

// Draw combines all the points to a printable sewing pattern
func (g TailoredShirtBlock) Draw(c *svg.SVG) {
	// 	g.shirt.Draw(c, 10, 70)
	// 	g.sleeve.Draw(c, 910, 20)
	// 	g.collar.Draw(c, 850, 770)
	// 	g.collarStand.Draw(c, 850, 820)
	// 	g.placked.Draw(c, 700, 650)

	g.testSquare.Draw(c, 50, 100)
}

// TailoredShirtBlock creates a map of points the enable the drawing of the tailored shirt
func (g *TailoredShirtBlock) setPartShirt(m Mesurments) {
	g.shirt = newPart("Shirt")
	g.shirt.pm.add(0, 0, "p0")
	g.shirt.pm.add(g.shirt.pm["p0"].x, g.shirt.pm["p0"].y+(m[SyncDepth]+60), "p1")       // chest Point
	g.shirt.pm.add(g.shirt.pm["p0"].x, g.shirt.pm["p0"].y+(m[BackNeckToWaist]+25), "p2") // waist Point
	g.shirt.pm.add(g.shirt.pm["p0"].x, g.shirt.pm["p0"].y+(m[ShirtLength]+40), "p3")     // hem Point
	g.shirt.pm.add(g.shirt.pm["p1"].x+((m[Chest]/2)+100), g.shirt.pm["p1"].y, "p4")      // chestwidth Point
	g.shirt.pm.add(g.shirt.pm["p4"].x, g.shirt.pm["p0"].y, "p5")
	g.shirt.pm.add(g.shirt.pm["p4"].x, g.shirt.pm["p3"].y, "p6")
	g.shirt.pm.add(g.shirt.pm["p0"].x+((m[NeckSize]/5)-5), g.shirt.pm["p0"].y, "p7")
	g.shirt.pm.addPoint(g.shirt.pm["p7"].diagonalLeftUp(20))
	g.shirt.pm.add(g.shirt.pm["p7"].x-(g.shirt.pm["p7"].distanceInt(g.shirt.pm["p0"])/2), g.shirt.pm["p7"].y, "p7a")

	g.shirt.pm.add(g.shirt.pm["p7"].x, g.shirt.pm["p7"].y-45, "p8")
	g.shirt.pm.add(g.shirt.pm["p7"].x, g.shirt.pm["p7"].y-(g.shirt.pm["p7"].distanceInt(g.shirt.pm["p8"])/2), "p7b")
	g.shirt.pm.add(g.shirt.pm["p0"].x, (g.shirt.pm["p0"].distanceInt(g.shirt.pm["p1"])/5)+40, "p9")
	g.shirt.pm.add(g.shirt.pm["p9"].x+m[HalfBack]+40, g.shirt.pm["p9"].y, "p10")
	g.shirt.pm.add(g.shirt.pm["p10"].x, g.shirt.pm["p1"].y, "p11")
	g.shirt.pm.addPoint(g.shirt.pm["p11"].diagonalRightUp(30))
	g.shirt.pm.add(g.shirt.pm["p10"].x, g.shirt.pm["p0"].y, "p12")
	g.shirt.pm.add(g.shirt.pm["p12"].x+15, g.shirt.pm["p12"].y, "p13")
	g.shirt.pm.add(g.shirt.pm["p13"].x, g.shirt.pm["p13"].y-20, "p14")
	g.shirt.pm.add(g.shirt.pm["p10"].x-100, g.shirt.pm["p10"].y, "p15")
	g.shirt.pm.add(g.shirt.pm["p10"].x, g.shirt.pm["p10"].y+7, "p16")
	g.shirt.pm.add(g.shirt.pm["p1"].x+(g.shirt.pm["p1"].distanceInt(g.shirt.pm["p4"])/2), g.shirt.pm["p1"].y, "p17")
	g.shirt.pm.add(g.shirt.pm["p17"].x, g.shirt.pm["p17"].y+g.shirt.pm["p1"].distanceInt(g.shirt.pm["p2"])+25, "p18")
	g.shirt.pm.add(g.shirt.pm["p17"].x, g.shirt.pm["p3"].y, "p19")
	g.shirt.pm.add(g.shirt.pm["p5"].x, g.shirt.pm["p5"].y+45, "p20")
	g.shirt.pm.add(g.shirt.pm["p20"].x-((m[NeckSize]/5)-10), g.shirt.pm["p20"].y, "p21")

	g.shirt.pm.add(g.shirt.pm["p20"].x, g.shirt.pm["p20"].y+((m[NeckSize]/5)-25), "p22")
	g.shirt.pm.add(g.shirt.pm["p21"].x+10, g.shirt.pm["p22"].y, "p22a")
	g.shirt.pm.addPoint(g.shirt.pm["p22a"].diagonalRightUp(10))
	g.shirt.pm.add(g.shirt.pm["p22"].x-(g.shirt.pm["p22ad"].distanceInt(g.shirt.pm["p22"])/2), g.shirt.pm["p22"].y, "p22b")
	g.shirt.pm.add(g.shirt.pm["p21"].x+(g.shirt.pm["p21"].distanceInt(g.shirt.pm["p22ad"])/10), g.shirt.pm["p22"].y-(g.shirt.pm["p21"].distanceInt(g.shirt.pm["p22ad"])/2), "p22c")
	g.shirt.pm.add(g.shirt.pm["p10"].x, g.shirt.pm["p10"].y+15, "p23")
	g.shirt.pm.add(g.shirt.pm["p10"].x, g.shirt.pm["p10"].y+g.shirt.pm["p23"].distanceInt(g.shirt.pm["p11"])/2, "p23a")
	g.shirt.pm.add(g.shirt.pm["p21"].x-(calculateBfromCandA(g.shirt.pm["p8"].distanceInt(g.shirt.pm["p14"]), g.shirt.pm["p23"].y-g.shirt.pm["p21"].y)+5), g.shirt.pm["p23"].y, "p24")
	g.shirt.pm.add(g.shirt.pm["p1"].x+((m[Chest]/3)+40), g.shirt.pm["p1"].y, "p25")
	g.shirt.pm.addPoint(g.shirt.pm["p25"].diagonalLeftUp(20))
	g.shirt.pm.add(g.shirt.pm["p25"].x, g.shirt.pm["p25"].y-40, "p26")
	g.shirt.pm.add(g.shirt.pm["p24"].x+20, g.shirt.pm["p24"].y+(g.shirt.pm["p24"].distanceInt(g.shirt.pm["p25"])/2), "p27")
	g.shirt.pm.add(g.shirt.pm["p22"].x+15, g.shirt.pm["p22"].y, "p28")
	g.shirt.pm.add(g.shirt.pm["p28"].x+35, g.shirt.pm["p28"].y, "p29")
	g.shirt.pm.add(g.shirt.pm["p18"].x+25, g.shirt.pm["p18"].y, "p30")
	g.shirt.pm.add(g.shirt.pm["p18"].x-25, g.shirt.pm["p18"].y, "p31")
	g.shirt.pm.add(g.shirt.pm["p19"].x, g.shirt.pm["p19"].y-80, "p32")
	g.shirt.pm.add(g.shirt.pm["p32"].x+15, g.shirt.pm["p32"].y, "p33")
	g.shirt.pm.add(g.shirt.pm["p32"].x-15, g.shirt.pm["p32"].y, "p34")
	g.shirt.pm.add(g.shirt.pm["p6"].x-(g.shirt.pm["p6"].distanceInt(g.shirt.pm["p19"])/2), g.shirt.pm["p3"].y, "p35")
	g.shirt.pm.add(g.shirt.pm["p35"].x, g.shirt.pm["p35"].y-30, "p36")
	g.shirt.pm.add(g.shirt.pm["p3"].x+(g.shirt.pm["p3"].distanceInt(g.shirt.pm["p19"])/2), g.shirt.pm["p3"].y, "p37")
	g.shirt.pm.add(g.shirt.pm["p1"].x+(g.shirt.pm["p1"].distanceInt(g.shirt.pm["p11"])/2), g.shirt.pm["p1"].y, "p38")
	g.shirt.pm.add(g.shirt.pm["p38"].x, g.shirt.pm["p38"].y+40, "p39")
	g.shirt.pm.add(g.shirt.pm["p39"].x, (g.shirt.pm["p2"].y + 25), "p40")
	g.shirt.pm.add(g.shirt.pm["p40"].x, g.shirt.pm["p40"].y+(m[ShirtLength]/5), "p41")
	g.shirt.pm.add(g.shirt.pm["p40"].x-7, g.shirt.pm["p40"].y, "p42")
	g.shirt.pm.add(g.shirt.pm["p40"].x+7, g.shirt.pm["p40"].y, "p43")

	g.shirt.pm.add(g.shirt.pm["p4"].x, g.shirt.pm["p36"].y, "p6a")
	g.shirt.pm.add(g.shirt.pm["p29"].x, g.shirt.pm["p36"].y, "p6b")
	g.shirt.pm.add(g.shirt.pm["p28"].x, g.shirt.pm["p6a"].y, "p6aa")

	// Add shirt back and front
	bf := newOutline("cuttingEdge", styleCuttingEdge)
	bf.add(
		g.shirt.pm["p0"], g.shirt.pm["p9"], g.shirt.pm["p15"], g.shirt.pm["p9"], g.shirt.pm["p3"],
		g.shirt.pm["p37"], g.shirt.pm["p34"], g.shirt.pm["p31"], g.shirt.pm["p17"], g.shirt.pm["p30"], g.shirt.pm["p33"], g.shirt.pm["p36"],
		g.shirt.pm["p6a"], g.shirt.pm["p6b"], g.shirt.pm["p29"], g.shirt.pm["p22"],
		g.shirt.pm["p22b"], g.shirt.pm["p22ad"], g.shirt.pm["p22c"], g.shirt.pm["p21"], g.shirt.pm["p24"], g.shirt.pm["p27"], g.shirt.pm["p26"], g.shirt.pm["p25d"],
		g.shirt.pm["p17"], g.shirt.pm["p11d"], g.shirt.pm["p23a"], g.shirt.pm["p23"], g.shirt.pm["p15"], g.shirt.pm["p10"], g.shirt.pm["p14"], g.shirt.pm["p8"],
		g.shirt.pm["p7d"], g.shirt.pm["p7a"], g.shirt.pm["p0"])
	g.shirt.addOutline(bf)

	// Add center line
	centerline := newOutline("center", styleCenterLine)
	centerline.add(g.shirt.pm["p6a"])
	centerline.add(g.shirt.pm["p22"])
	g.shirt.addOutline(centerline)

	// Add Dart
	d := newOutline("dart", styleDart)
	d.add(g.shirt.pm["p39"], g.shirt.pm["p42"], g.shirt.pm["p41"], g.shirt.pm["p43"], g.shirt.pm["p39"])
	g.shirt.addOutline(d)

	// Add Foldline
	fl := newOutline("foldline", styleFoldline)
	fl.add(g.shirt.pm["p28"], g.shirt.pm["p6aa"])
	g.shirt.addOutline(fl)
}

func (g *TailoredShirtBlock) setPointsSleeve(m Mesurments) {
	// Getting the armsync from the shirt arm hole
	armSync := int(math.Round((g.shirt.pm["p14"].distance(g.shirt.pm["p10"]) +
		g.shirt.pm["p23"].distance(g.shirt.pm["p23a"]) +
		g.shirt.pm["p23a"].distance(g.shirt.pm["p11d"]) +
		g.shirt.pm["p11d"].distance(g.shirt.pm["p17"]) +
		g.shirt.pm["p17"].distance(g.shirt.pm["p25d"]) +
		g.shirt.pm["p25d"].distance(g.shirt.pm["p26"]) +
		g.shirt.pm["p26"].distance(g.shirt.pm["p27"]) +
		g.shirt.pm["p27"].distance(g.shirt.pm["p24"]))))

	g.sleeve = newPart("Sleeve")

	g.sleeve.pm.add(0, 0, "p0")
	g.sleeve.pm.add(g.sleeve.pm["p0"].x, g.sleeve.pm["p0"].y+((armSync/4)+15), "p1")
	g.sleeve.pm.add(g.sleeve.pm["p0"].x, g.sleeve.pm["p0"].y+(m[SleeveLength]+60-m[CuffDepth]-(g.shirt.pm["p0"].distanceInt(g.shirt.pm["p13"]))), "p2")
	//g.sleeve.pm.add(g.sleeve.pm["p0"].x, g.sleeve.pm["p0"].y + (m[SleeveLength] + 60 - m[CuffDepth]), "p2")
	g.sleeve.pm.add(g.sleeve.pm["p1"].x, g.sleeve.pm["p0"].y+(g.sleeve.pm["p1"].distanceInt(g.sleeve.pm["p2"])/2), "p3")
	// Schuin e streep met bepaalde lengte = √(c²−a²)
	p1p4Distance := calculateBfromCandA((armSync/2)-5, (g.sleeve.pm["p0"].distanceInt(g.sleeve.pm["p1"])))

	g.sleeve.pm.add(g.sleeve.pm["p1"].x-p1p4Distance, g.sleeve.pm["p1"].y, "p4")
	g.sleeve.pm.add(g.sleeve.pm["p4"].x, g.sleeve.pm["p2"].y, "p5")
	g.sleeve.pm.add(g.sleeve.pm["p1"].x+p1p4Distance, g.sleeve.pm["p1"].y, "p6")
	g.sleeve.pm.add(g.sleeve.pm["p6"].x, g.sleeve.pm["p2"].y, "p7")
	g.sleeve.pm.add(g.sleeve.pm["p0"].x-((g.sleeve.pm["p1"].distanceInt(g.sleeve.pm["p4"])/4)*3), g.sleeve.pm["p1"].y-((g.sleeve.pm["p0"].distanceInt(g.sleeve.pm["p1"])/4)*1), "p8")
	g.sleeve.pm.addPoint(g.sleeve.pm["p8"].diagonalRightDown(5))
	g.sleeve.pm.add(g.sleeve.pm["p0"].x-((g.sleeve.pm["p1"].distanceInt(g.sleeve.pm["p4"])/4)*2), g.sleeve.pm["p1"].y-((g.sleeve.pm["p0"].distanceInt(g.sleeve.pm["p1"])/4)*2), "p9")
	g.sleeve.pm.addPoint(g.sleeve.pm["p9"].diagonalLeftUp(12))
	g.sleeve.pm.add(g.sleeve.pm["p0"].x-((g.sleeve.pm["p1"].distanceInt(g.sleeve.pm["p4"])/4)*1), g.sleeve.pm["p1"].y-((g.sleeve.pm["p0"].distanceInt(g.sleeve.pm["p1"])/4)*3), "p10")
	g.sleeve.pm.addPoint(g.sleeve.pm["p10"].diagonalLeftUp(22))
	g.sleeve.pm.add(g.sleeve.pm["p0"].x+((g.sleeve.pm["p1"].distanceInt(g.sleeve.pm["p6"])/4)*3), g.sleeve.pm["p1"].y-((g.sleeve.pm["p0"].distanceInt(g.sleeve.pm["p1"])/4)*1), "p13")
	g.sleeve.pm.addPoint(g.sleeve.pm["p13"].diagonalLeftDown(12))
	g.sleeve.pm.add(g.sleeve.pm["p0"].x+((g.sleeve.pm["p1"].distanceInt(g.sleeve.pm["p6"])/4)*2), g.sleeve.pm["p1"].y-((g.sleeve.pm["p0"].distanceInt(g.sleeve.pm["p1"])/4)*2), "p12")
	g.sleeve.pm.add(g.sleeve.pm["p0"].x+((g.sleeve.pm["p1"].distanceInt(g.sleeve.pm["p6"])/4)*1), g.sleeve.pm["p1"].y-((g.sleeve.pm["p0"].distanceInt(g.sleeve.pm["p1"])/4)*3), "p11")
	g.sleeve.pm.addPoint(g.sleeve.pm["p11"].diagonalRightUp(15))
	g.sleeve.pm.add(g.sleeve.pm["p5"].x+((g.sleeve.pm["p5"].distanceInt(g.sleeve.pm["p2"])/3)+7), g.sleeve.pm["p5"].y, "p14")
	g.sleeve.pm.add(g.sleeve.pm["p7"].x-((g.sleeve.pm["p7"].distanceInt(g.sleeve.pm["p2"])/3)+7), g.sleeve.pm["p5"].y, "p15")
	g.sleeve.pm.add(g.sleeve.pm["p14"].x+(g.sleeve.pm["p2"].distanceInt(g.sleeve.pm["p14"])/2), g.sleeve.pm["p14"].y, "p16")
	g.sleeve.pm.add(g.sleeve.pm["p16"].x, g.sleeve.pm["p16"].y-(m[SleeveLength]/6), "p17")

	s := newOutline("cuttingedge", styleCuttingEdge)
	s.add(g.sleeve.pm["p0"], g.sleeve.pm["p10d"], g.sleeve.pm["p9d"], g.sleeve.pm["p8d"], g.sleeve.pm["p4"], g.sleeve.pm["p14"],
		g.sleeve.pm["p16"], g.sleeve.pm["p17"], g.sleeve.pm["p16"], g.sleeve.pm["p15"], g.sleeve.pm["p6"], g.sleeve.pm["p13d"], g.sleeve.pm["p12"],
		g.sleeve.pm["p11d"], g.sleeve.pm["p0"])
	s.addGravity(g.sleeve.pm["p0"].x-5, g.sleeve.pm["p6"].y+30)
	g.sleeve.addOutline(s)
	g.sleeve.addOutline(s.calculateHemPoints())

}

func (g *TailoredShirtBlock) setPointsCollar(m Mesurments) {

	halfNeckline := g.shirt.pm["p0"].distanceInt(g.shirt.pm["p7a"]) + // P0 Center back
		g.shirt.pm["p7a"].distanceInt(g.shirt.pm["p7d"]) +
		g.shirt.pm["p7d"].distanceInt(g.shirt.pm["p8"]) +
		g.shirt.pm["p21"].distanceInt(g.shirt.pm["p22c"]) +
		g.shirt.pm["p22c"].distanceInt(g.shirt.pm["p22ad"]) +
		g.shirt.pm["p22ad"].distanceInt(g.shirt.pm["p22b"]) +
		g.shirt.pm["p22b"].distanceInt(g.shirt.pm["p22"])

	// The neck line above is mesured with seam allowance.
	// To get the real fitting line the seamallowance should be substracted
	// So adding 9% (taking the outer longer curve), will take care of this.
	halfNeckline = halfNeckline + (halfNeckline / 9)
	buttonstand := g.shirt.pm["p22"].distanceInt(g.shirt.pm["p28"])

	g.collar = newPart("Collar")

	g.collar.pm.add(0, 0, "p1")
	g.collar.pm.add(g.collar.pm["p1"].x+halfNeckline, g.collar.pm["p1"].y, "p2")
	g.collar.pm.add(g.collar.pm["p2"].x+buttonstand+12, g.collar.pm["p2"].y, "p3")
	g.collar.pm.add(g.collar.pm["p1"].x+((g.collar.pm["p1"].distanceInt(g.collar.pm["p2"])/4)*3), g.collar.pm["p1"].y, "p4")

	g.collar.pm.add(g.collar.pm["p1"].x, g.collar.pm["p1"].y-(m[CollarDepth]*2)+20, "p5") // +20 is the seam allowance top collor and neck line
	g.collar.pm.add(g.collar.pm["p1"].x, g.collar.pm["p1"].y-(g.collar.pm["p1"].distanceInt(g.collar.pm["p5"])/2), "p6")
	g.collar.pm.add(g.collar.pm["p6"].x, g.collar.pm["p6"].y+10, "p6a") // Seam allowance for the colar
	g.collar.pm.add(g.collar.pm["p6"].x, g.collar.pm["p6"].y-10, "p6b") // Seam allowance for the stand
	g.collar.pm.add(g.collar.pm["p2"].x, g.collar.pm["p6"].y, "p7")
	g.collar.pm.add(g.collar.pm["p7"].x+10, g.collar.pm["p5"].y-5, "p16") // Colar Point
	g.collar.pm.add(g.collar.pm["p3"].x, g.collar.pm["p6"].y, "p8")
	g.collar.pm.add(g.collar.pm["p8"].x-10, g.collar.pm["p8"].y, "p9")
	g.collar.pm.add(g.collar.pm["p3"].x, g.collar.pm["p3"].y-8, "p10")
	g.collar.pm.add(g.collar.pm["p9"].x, g.collar.pm["p9"].y+8, "p11")
	g.collar.pm.add(g.collar.pm["p1"].x, g.collar.pm["p1"].y-5, "p12")
	g.collar.pm.add(g.collar.pm["p12"].x+10, g.collar.pm["p12"].y, "p12a")
	g.collar.pm.add(g.collar.pm["p4"].x-g.collar.pm["p12"].distanceInt(g.collar.pm["p4"])/2, g.collar.pm["p4"].y, "p4a") // Helps out to smoothen curve
	g.collar.pm.add(g.collar.pm["p6"].x+(g.collar.pm["p6"].distanceInt(g.collar.pm["p7"])/2), g.collar.pm["p6"].y, "p13")
	g.collar.pm.add(g.collar.pm["p13"].x, g.collar.pm["p13"].y+10, "p13a") // Seam allowance for the colar
	g.collar.pm.add(g.collar.pm["p13"].x, g.collar.pm["p13"].y-10, "p13b") // Seam allowance for the stand
	g.collar.pm.add(g.collar.pm["p7"].x, g.collar.pm["p7"].y+10, "p14")
	g.collar.pm.add(g.collar.pm["p14"].x, g.collar.pm["p14"].y+10, "p14a") // Seam allowance for the colar
	g.collar.pm.add(g.collar.pm["p14"].x, g.collar.pm["p14"].y-10, "p14b") // Seam allowance for the stand
	g.collar.pm.add(g.collar.pm["p11"].x+6, g.collar.pm["p11"].y+8, "p15") // !!Hier moet het anders berekend worden. p9 -> p10 diagonale -10 == p15
	g.collar.pm.add(g.collar.pm["p15"].x, g.collar.pm["p15"].y-10, "p15b") // Seam allowance for the stand

	c := newOutline("cuttingedge", styleCuttingEdge)
	c.addGravity(g.collar.pm["p5"].x+120, g.collar.pm["p5"].y+20)
	c.add(g.collar.pm["p6a"],
		g.collar.pm["p13a"],
		g.collar.pm["p14a"],
		g.collar.pm["p16"],
		g.collar.pm["p5"],
		g.collar.pm["p6a"])

	g.collar.addOutline(c)
	g.collar.addOutline(c.calculateHemPoints())

	g.collarStand = newPart("CollarStand")
	g.collarStand.pm = g.collar.pm
	cs := newOutline("cuttingedge", styleCuttingEdge)
	cs.add(g.collarStand.pm["p12"],
		g.collarStand.pm["p12a"],
		g.collarStand.pm["p4a"],
		g.collarStand.pm["p4"],
		g.collarStand.pm["p10"],
		g.collarStand.pm["p15b"],
		g.collarStand.pm["p14b"],
		g.collarStand.pm["p13b"],
		g.collarStand.pm["p6b"],
		g.collarStand.pm["p12"])
	cs.addGravity(g.collarStand.pm["p13b"].x+5, g.collarStand.pm["p15b"].y+10)
	g.collarStand.addOutline(cs)
	g.collarStand.addOutline(cs.calculateHemPoints())
}

func (g *TailoredShirtBlock) setPlacked(m Mesurments) {
	g.placked = newPart("Placked")
	// links boven hoek
	g.placked.pm.add(0, 50, "p0")
	// naar onder (m[SleeveLength] / 6) +10
	g.placked.pm.add(g.placked.pm["p0"].x, g.placked.pm["p0"].y+(m[SleeveLength]/6)+10, "p1")
	// naar rechts
	g.placked.pm.add(g.placked.pm["p1"].x+30, g.placked.pm["p1"].y, "p2")
	g.placked.pm.add(g.placked.pm["p2"].x+10, g.placked.pm["p1"].y, "p3")
	g.placked.pm.add(g.placked.pm["p3"].x+65, g.placked.pm["p3"].y, "p4")
	// naar boven
	g.placked.pm.add(g.placked.pm["p4"].x, g.placked.pm["p4"].y-((m[SleeveLength]/6)+50), "p5")
	// bij de bovenkant naar links
	g.placked.pm.add(g.placked.pm["p5"].x-50, g.placked.pm["p5"].y, "p6")
	// stukje naar onder
	g.placked.pm.add(g.placked.pm["p6"].x, g.placked.pm["p6"].y+40, "p7")
	// naar links tot de "box"
	g.placked.pm.add(g.placked.pm["p7"].x-15, g.placked.pm["p7"].y, "p8")
	g.placked.pm.add(g.placked.pm["p8"].x-10, g.placked.pm["p8"].y, "p9")

	// De boven kant van "the box" markeren
	g.placked.pm.add(g.placked.pm["p8"].x, g.placked.pm["p8"].y+10, "p11")
	g.placked.pm.add(g.placked.pm["p9"].x, g.placked.pm["p9"].y+10, "p12")

	// Adding the outline
	p := newOutline("cuttingedge", styleCuttingEdge)
	p.add(
		g.placked.pm["p0"],
		g.placked.pm["p1"],
		g.placked.pm["p4"],
		g.placked.pm["p5"],
		g.placked.pm["p6"],
		g.placked.pm["p7"],
		g.placked.pm["p0"],
	)
	p.addGravity(g.placked.pm["p4"].x-15, g.placked.pm["p1"].y/2)
	g.placked.addOutline(p)
	g.placked.addOutline(p.calculateHemPoints())

	// Adding "the box" in the placked
	pb := newOutline("box", styleHem)
	pb.add(g.placked.pm["p3"],
		g.placked.pm["p11"],
		g.placked.pm["p12"],
		g.placked.pm["p2"],
	)
	g.placked.addOutline(pb)

}

func (g *TailoredShirtBlock) setTestSquare() {
	g.testSquare = newPart("square")

	g.testSquare.pm.add(0, 20, "p0")

	g.testSquare.pm.add(0, 40, "p1")
	g.testSquare.pm.add(-15, 60, "p2")
	g.testSquare.pm.add(0, 80, "p3")

	g.testSquare.pm.add(0, 120, "p4")
	g.testSquare.pm.add(40, 120, "p5")
	g.testSquare.pm.add(40, 60, "p6")
	g.testSquare.pm.add(60, 40, "p7")
	g.testSquare.pm.add(80, 40, "p8")
	g.testSquare.pm.add(100, 60, "p9")
	g.testSquare.pm.add(100, 120, "p10")
	g.testSquare.pm.add(150, 120, "p11")

	g.testSquare.pm.add(150, 80, "p12")
	g.testSquare.pm.add(165, 60, "p13")
	g.testSquare.pm.add(150, 40, "p14")

	g.testSquare.pm.add(150, 20, "p15")
	g.testSquare.pm.add(130, 0, "p16")
	g.testSquare.pm.add(110, 0, "p17")
	g.testSquare.pm.add(110, -30, "p18")
	g.testSquare.pm.add(40, -30, "p19")
	g.testSquare.pm.add(40, 0, "p20")
	g.testSquare.pm.add(20, 0, "p21")

	sq := newOutline("square", styleCuttingEdge)
	sq.add(g.testSquare.pm["p0"])
	sq.add(g.testSquare.pm["p1"])
	sq.add(g.testSquare.pm["p2"])
	sq.add(g.testSquare.pm["p3"])
	sq.add(g.testSquare.pm["p4"])
	sq.add(g.testSquare.pm["p5"])
	sq.add(g.testSquare.pm["p6"])
	sq.add(g.testSquare.pm["p7"])
	sq.add(g.testSquare.pm["p8"])
	sq.add(g.testSquare.pm["p9"])
	sq.add(g.testSquare.pm["p10"])
	sq.add(g.testSquare.pm["p11"])

	sq.add(g.testSquare.pm["p12"])
	sq.add(g.testSquare.pm["p13"])
	sq.add(g.testSquare.pm["p14"])
	sq.add(g.testSquare.pm["p15"])
	sq.add(g.testSquare.pm["p16"])
	sq.add(g.testSquare.pm["p17"])
	sq.add(g.testSquare.pm["p18"])
	sq.add(g.testSquare.pm["p19"])
	sq.add(g.testSquare.pm["p20"])
	sq.add(g.testSquare.pm["p21"])

	sq.add(g.testSquare.pm["p0"])
	sq.addGravity(29, 30)
	sq.addGravity(105, 30)
	sq.addGravity(70, 1)

	g.testSquare.addOutline(sq)
	g.testSquare.addOutline(sq.calculateHemPoints())
}
