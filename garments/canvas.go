package garments

import (
	"log"

	svg "github.com/ajstarks/svgo"
)

// pointNrToCanvas adds the Point numbers on the canvas
func pointNrToCanvas(ol Outline, c *svg.SVG) {
	if ol.name == "hem" {
		return
	}
	for _, p := range ol.list {
		if len(p.name) == 0 {
			log.Fatal("Empty Point")
		}
		c.Text(p.x+2, p.y-2, p.name)
	}
}

func addTextNearPoint(p Point, offsetx int, offsety int, s string, c *svg.SVG) {
	c.Text(p.x+offsetx, p.y+offsety, s)
}

func addTextVerticalNearPoint(p Point, offsetx int, offsety int, s string, c *svg.SVG) {
	c.Text(p.x+offsetx, p.y+offsety, s, "style=\"writing-mode: tb;\"")
}

// addDotsToCancas adds dots of the Point's on the canvas
func addDotsToCanvas(ol Outline, c *svg.SVG, s ...string) {
	for _, p := range ol.list {
		if len(p.name) == 0 {
			log.Fatal("Empty Point")
		}
		c.Circle(p.x, p.y, 1, s...)
	}
}

// pointsToPolyline drawes lines from a slice of points on the canvas
func pointsToPolyline(ol Outline, c *svg.SVG, s ...string) {
	dX := make([]int, 0)
	dY := make([]int, 0)
	for _, p := range ol.list {
		if len(p.name) == 0 {
			log.Fatal("Empty Point")
		}
		dX = append(dX, p.x)
		dY = append(dY, p.y)
	}
	c.Polyline(dX, dY, s...)
}
