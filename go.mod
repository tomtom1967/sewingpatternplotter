module bitbucket.org/tomtom1967/sewingpatternplotter

require (
	github.com/ajstarks/svgo v0.0.0-20181006003313-6ce6a3bcf6cd
	github.com/google/go-cmp v0.3.0
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	golang.org/x/tools v0.0.0-20190411180116-681f9ce8ac52 // indirect
)
