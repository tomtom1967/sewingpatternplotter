# Iemand de maat nemen:

* **A-B: Nek naar taille** Meet van af de bot bobbel in de nek, naar de taille lijn **G**.
* **A-C: Gewenste shirt lengte** Meet van af de bot bobbel in de nek, tot aan de gewenste lengte van het kledingstuk
* **D-E: Halve rug breedte** Meet 15cm onder de bot bobbel in de nek de rug breedte van mouwnaad tot mouwnaad, en deel door 2
* **F: Borst omvang** (belangrijkste meeting) Onder de okseles, *over* de schouder bladen, rondom de borst.
* **G: Natuurlijke taille** Het smalste punt in de taille
* **H: Broek taille** Ongeveer 5cm onder G, die positie waar normaal de broek gedragen wordt.
* **I: Heup / zitvlak** Meet om het breedste gedeeltje van de heup / zitvlak.
* **J: Nek** Meet zo laag mogelijk (onderkant meetlint) en losjes om de basis van de nek.
* **K-L: Schouder** Meet van af de basis van de nek tot aan het schouderbotje. (alleen bij extreem breede schouders.)
* **L-M: Mouw lengte 1** (voor mouw uit 1 deel) Meet van af schouderbotje tot aan polsbotje.
* **D-N: Mouw lengte 2** (voor mouw uit 2 delen) Doe de arm 90gr opzij en buig elleboog. Meet van af midden rug (15cm onder nek botje), over de elleboog, tot pols botje.
* **D-O: Elleboog lengte** (Alleen bij extreem langearm) Van elleboog tot pols botje
* **P-Q: Zijnaad broek** Meet van af de taile tot aan de grond.
* **R-S: Zit hoogte** Meet van af de *broek taille* tot aan de zitting.
* **T-U: Binnebeen lengte** Meet van af het kruis tot aan tot aan de grond
* **V: Pols omvang** Meet net boven het polsbotje

## Extra metingen
* **W: Armomvang** Meet de bovenkant van de arm die licht gebogen is. Alleen nodig bij erg ontwikelde bovenarm.
* **A-X: Scye dieptje** Gebruik de standaarden van de maattabellen, alleen meten wanneer deze te kort blijkt.

**NOTE:** A-X Scye diepte blijkt moeilijk te meten. De standaard waarde die wordt gebruikt bij een borst omvang van 92 cm is: **22.8cm** Bij grotere mannen doe **+1** Bij kleinere mannen doe **-1**

Bij het vergelijken van de genomen maat met de standaard tabellen moet je uitgaan van (F) de borstomvang. Bij grote afwijkingen is het vanbelang om nog een keer goed te meten.

Voor een overhemd zijn de maten A-B, A-C, D-E, F, G, J, L-M en eventueel A-X nodig


