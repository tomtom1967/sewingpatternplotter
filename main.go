//go:generate stringer -type=garments.MesurmentType
package main

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/tomtom1967/sewingpatternplotter/garments"
	svg "github.com/ajstarks/svgo"
)

// PRINTING
// Van svg naar eps exporteren via inkscape
// 🌊 inkscape bla.svg -E testT.eps

// Van eps een poster eps maken zie commando hieronder
// Van de poster eps een pdf maken
// Klaar

// Maak een poster van 800*1000mm op A4tjes tot A0 formaat
// ▶ poster -v -i800x1000mm -mA4 -pA0 pattern.eps > poster.eps

// Maakt een poster van 1582x1089mm op een schaal van 1:1
// ▶ poster -v -i1582x1089mm -mA4 -s1 bla.eps > poster.eps

// A0 841 x 1189 mm
// 2A0 1189 x 1682 mm
// The width and height of the svg file in landscape
const width = 1300
const height = 1000

// step is an element that asks user input un the command line
type step struct {
	key         garments.MesurmentType
	description string
	value       interface{}
	collection  string
}

func main() {

	dir, _ := filepath.Abs("./")

	// Commandline steps to ask commandline info step by step
	ss := []step{
		{key: garments.Name, description: "Name", value: "Unknown", collection: "info"},
		{key: garments.Date, description: "Date (press enter for [%s])", value: time.Now().Format("20060102"), collection: "info"},

		{key: garments.NeckSize, description: "Neck size in mm (J)\n Mesure easily around the neck.", value: 400, collection: "mesurments"},
		{key: garments.Chest, description: "Chest size in mm (F)\n Mesure over the shoulder blades", value: 1000, collection: "mesurments"},
		{key: garments.SyncDepth, description: "Sync depth length in mm (A-X)", value: 244, collection: "mesurments"},
		{key: garments.BackNeckToWaist, description: "Back neck to waist length in mm (A-B)", value: 442, collection: "mesurments"},
		{key: garments.HalfBack, description: "Half of the back width in mm (D-E)", value: 200, collection: "mesurments"},
		{key: garments.ShirtLength, description: "Shirt length in mm (A-C)", value: 810, collection: "mesurments"},
		{key: garments.SleeveLength, description: "Sleeve length in mm (L-M)", value: 860, collection: "mesurments"},
		{key: garments.CuffSize, description: "Cuff size in mm (V)", value: 240, collection: "mesurments"},
		{key: garments.CuffDepth, description: "Cuff depth in mm (def 60 mm) (V) ", value: 60, collection: "mesurments"},
		{key: garments.CollarDepth, description: "Collar depth in mm (def 30 mm)", value: 50, collection: "mesurments"},
	}

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Sewing Patter Shell")
	fmt.Println("---------------------")

	// Store the mesurments from the command line
	m := make(garments.Mesurments)
	// Store the text from the command line
	i := make(garments.Info)

	// Looping the input steps, asking for input
	for _, k := range ss {
		switch k.key {
		case garments.Date:
			fmt.Printf(k.description, time.Now().Format("20060102"))
		default:
			fmt.Printf(k.description + ": ")
		}

		text, _ := reader.ReadString('\n')
		text = strings.Replace(text, "\n", "", -1)
		switch k.collection {
		case "mesurments":
			if len(text) > 0 {
				m[k.key], _ = strconv.Atoi(text)
			} else {
				m[k.key] = k.value.(int)
			}

		case "info":
			if len(text) > 0 {
				i[k.key] = text
			} else {
				i[k.key] = k.value.(string)
			}
		}
	}

	// Create a file
	f, err := os.Create(dir + "/" + i[garments.Name] + "-" + i[garments.Date] + ".svg")
	if err != nil {
		fmt.Printf("Error %s", err)
	}
	defer f.Sync()
	defer f.Close()

	ts := garments.NewTailoredShirtBlock(m)

	// Create the canvas
	canvas := svg.New(f)
	canvas.StartviewUnit(ts.CanvasWidth(), ts.CanvasHeight(), "mm", 0, 0, ts.CanvasWidth(), ts.CanvasHeight())
	canvas.Rect(0, 0, ts.CanvasWidth(), ts.CanvasHeight(), `fill="white"`)
	canvas.Grid(0, 0, ts.CanvasWidth(), ts.CanvasHeight(), 100, "stroke:black;opacity:0.1")

	ts.Draw(canvas)
	canvas.End()

} // END OF MAIN
